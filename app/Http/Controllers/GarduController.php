<?php

namespace App\Http\Controllers;

use App\Models\Gardu;
use App\Models\GarduImport;
use App\Models\Unit;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class GarduController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request, $single)
    {
        if ($single == 'false') {
            $this->validate($request, [
                'file' => 'required|mimes:csv,xls,xlsx',
            ]);
            $file = $request->file('file');
            $filename = md5($file->getClientOriginalName()) . ('.') . $file->getClientOriginalExtension();
            $path = $file->storeAs('public/imports', $filename);
            $data['dataGardu'] = Excel::toArray(new GarduImport, storage_path('app/public/imports/' . $filename));
            return view('add-gardu-kolektif', $data);
        } else {
            $unit = Unit::where('code', 41610)->first();
            $temp = explode('-', str_replace(" ", "", $request->nometer));
            $temp1 = explode('-', str_replace(" ", "", $request->code));
            $gardu = new Gardu();
            $gardu->code = $temp1[0];
            $gardu->name = $request->name;
            $gardu->daya = (int) $temp1[1];
            $gardu->nometer = (int) $temp[0];
            $gardu->fkm = (int) $temp[1];
            $gardu->merkmeter = $request->merkmeter;
            $gardu->lat = (float) $request->lat; // check if after dot (.) musth 6 char
            $gardu->lng = (float) $request->lng; // check if after dot (.) musth 6 char
            $gardu->unit_id = $unit->id;
            $gardu->before = $request->asset_before;
            $gardu->next = $request->asset_next;

            $saved = $gardu->save();

            if (!$saved) {
                $data['stat'] = false;
            } else {
                $data['stat'] = true;
            }
            return view('add-gardu', $data);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Gardu $gardu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Gardu $gardu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Gardu $gardu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Gardu $gardu)
    {
        //
    }

    public function formatFile()
    {
        $file = public_path('file/format_aset_oml.xlsx');
        return response()->download($file);
    }
}
