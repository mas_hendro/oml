<?php

namespace App\Http\Controllers;

use App\Models\TitikUkur;
use App\Models\Unit;
use Illuminate\Http\Request;

class TitikUkurController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request, $single)
    {
        if ($single == 'false') {
            //
        } else {
            $unit = Unit::where('code', 41610)->first();
            $asset = new TitikUkur();
            $asset->name = $request->name;
            $asset->type = $request->type;
            $asset->lat = (float) $request->x_cord; // check if after dot (.) musth 6 char
            $asset->lng = (float) $request->y_cord; // check if after dot (.) musth 6 char
            $asset->before = $request->min_1;
            $asset->after = $request->plus_1;
            $asset->unit_id = $unit->id;

            $saved = $asset->save();

            if (!$saved) {
                $data['stat'] = false;
            } else {
                $data['stat'] = true;
            }
            return view('add-exim', $data);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(TitikUkur $titikUkur)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TitikUkur $titikUkur)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, TitikUkur $titikUkur)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TitikUkur $titikUkur)
    {
        //
    }
}
