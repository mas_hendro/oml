<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\Models\GarduImportModel;


class GarduImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        return new GarduImportModel([
            'unitul' => $row['unitul'],
            'kode_gardu' => $row['kode_gardu'],
            'nama_gardu' => $row['nama_gardu'],
            'daya' => $row['daya'],
            'fkm' => $row['fkm'],
            'merk_meter_terpasang' => $row['merk_meter_terpasang'],
            'no_meter_terpasang' => $row['no_meter_terpasang'],
            'lat' => $row['lat'],
            'long' => $row['long'],
            'aset_sebelum' => $row['aset_sebelum'],
            'aset_sesudah' => $row['aset_sesudah']
        ]);
    }

    public function headingRow(): int
    {
        return 1;
    }

    public function startRow(): int
    {
        return 3;
    }
}
