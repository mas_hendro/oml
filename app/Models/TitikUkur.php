<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TitikUkur extends Model
{
    use HasFactory;

    protected $table = 'titik_ukurs';
    protected $fillable = ['name', 'type', 'lat', 'lng', 'before', 'after', 'unit_id'];
    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }
}
