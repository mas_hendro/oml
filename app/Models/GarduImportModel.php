<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GarduImportModel extends Model
{
    protected $fillabble = ['unitul', 'kode_gardu', 'nama_gardu', 'daya', 'fkm', 'merk_meter_terpasang', 'no_meter_terpasang', 'lat', 'long', 'aset_sebelum', 'aset_sesudah'];
}
