<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use HasFactory;

    public function titikukur()
    {
        return $this->hasMany(TitikUkur::class);
    }

    public function gardu()
    {
        return $this->hasMany(Gardu::class);
    }
}
