<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gardu extends Model
{
    use HasFactory;

    protected $table = 'gardus';
    protected $fillable = ['code', 'name', 'daya', 'ct', 'nometer', 'merkmeter', 'unit_id'];
    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }
}
