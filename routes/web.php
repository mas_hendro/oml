<?php

use App\Http\Controllers\GarduController;
use App\Http\Controllers\TitikUkurController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

// search
Route::get('search', function () {
    return view('search');
});

// anev
Route::get('anev/losses-jtm', function () {
    return view('losses-jtm');
});
Route::get('anev/losses-gardu', function () {
    return view('losses-gardu');
});
Route::get('anev/unbalance', function () {
    return view('underconstruction');
});

// input
Route::get('pengukuran/satuan', function () {
    return view('input-stand-satuan');
});
Route::get('pengukuran/kolektif', function () {
    return view('input-stand-kolektif');
});

// add titik ukur
Route::get('titikukur/add/exim', function () {
    return view('add-exim');
});
Route::post('titikukur/add/exim/save/{single}', [TitikUkurController::class, 'create'])->name('titikukur.create');
Route::get('titikukur/add/gardu/satuan', function () {
    return view('add-gardu');
});
Route::post('titikukur/add/gardu/satuan/{single}', [GarduController::class, 'create'])->name('gardu.create-single');
Route::get('titikukur/add/gardu/kolektif', function () {
    return view('add-gardu-kolektif');
});
Route::get('titikukur/add/gardu/format_upload', [GarduController::class, 'formatFile'])->name('gardu.format-file');

// data titik ukur
