<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
  <!-- Sidebar - Brand -->
  @include('brand-sidebar')
  <!-- Divider -->
  <hr class="sidebar-divider my-0">
  <!-- Nav Item - Dashboard -->
  <li class="nav-item {{Request::is('/') ? 'active':''}}">
    <a class="nav-link" href="{{url('/')}}">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Overview</span></a>
  </li>
  <!-- Divider -->
  <hr class="sidebar-divider">
  <!-- Heading -->
  <div class="sidebar-heading">
    Menu
  </div>
  <!-- Nav Item - Tables -->
  <li class="nav-item {{Request::is('search') ? 'active':''}}">
    <a class="nav-link" href="{{url('search')}}">
      <i class="fas fa-fw fa-search"></i>
      <span>Cari Titik Ukur</span></a>
  </li>
  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item {{Request::is('anev*') ? 'active':''}}">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#anev" aria-expanded="true"
      aria-controls="collapsePages">
      <i class="fas fa-fw fa-clipboard"></i>
      <span>Analisa dan Evaluasi</span>
    </a>
    <div id="anev" class="collapse {{Request::is('anev/*') ? 'show':''}}" aria-labelledby="headingPages"
      data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Analisa dan Evaluasi:</h6>
        <a class="collapse-item {{Request::is('anev/losses-jtm*') ? 'active':''}}"
          href="{{ url('anev/losses-jtm')}}">Losses JTM</a>
        <a class="collapse-item {{Request::is('anev/losses-gardu*') ? 'active':''}}"
          href="{{ url('anev/losses-gardu')}}">Losses Gardu</a>
        <a class="collapse-item {{Request::is('anev/unbalance*') ? 'active':''}}"
          href="{{ url('anev/unbalance') }}">Unbalance
          Gardu</a>
      </div>
    </div>
  </li>
  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item {{Request::is('pengukuran/*') ? 'active':''}}">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#inputPemeriksaan" aria-expanded="true"
      aria-controls="collapsePages">
      <i class="fas fa-fw fa-pencil-alt"></i>
      <span>Input Stand kWh</span>
    </a>
    <div id="inputPemeriksaan" class="collapse {{Request::is('pengukuran/*') ? 'show':''}}"
      aria-labelledby="headingPages" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Input Stand:</h6>
        <a class="collapse-item {{Request::is('pengukuran/satuan') ? 'active':''}}"
          href="{{ url('pengukuran/satuan') }}">Satuan</a>
        <a class="collapse-item {{Request::is('pengukuran/kolektif') ? 'active':''}}"
          href="{{ url('pengukuran/kolektif') }}">Kolektif</a>
      </div>
    </div>
  </li>
  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item {{Request::is('gardu/tambah*')  ? 'active':''}}">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#tambahGardu" aria-expanded="true"
      aria-controls="collapsePages">
      <i class="fas fa-fw fa-plus-circle"></i>
      <span>Tambah Titik Transaksi</span>
    </a>
    <div id="tambahGardu" class="collapse {{Request::is('titikukur/add/*') ? 'show':''}}" aria-labelledby="headingPages"
      data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Pangkal / Ujung:</h6>
        <a class="collapse-item {{Request::is('titikukur/add/exim*') ? 'active':''}}"
          href="{{url('titikukur/add/exim')}}">Satuan</a>
        <h6 class="collapse-header">Gardu:</h6>
        <a class="collapse-item {{Request::is('titikukur/add/gardu/satuan*') ? 'active':''}}"
          href="{{url('titikukur/add/gardu/satuan')}}">Satuan</a>
        <a class="collapse-item {{Request::is('titikukur/add/gardu/kolektif*') ? 'active':''}}"
          href="{{url('titikukur/add/gardu/kolektif')}}">Kolektif</a>
      </div>
    </div>
  </li>
  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item {{Request::is('gardu') ? 'active':''}}">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#perUlp" aria-expanded="true"
      aria-controls="collapsePages">
      <i class="fas fa-fw fa-folder"></i>
      <span>Master Data</span>
    </a>
    <div id="perUlp" class="collapse {{Request::is('gardu') ? 'show':''}}" aria-labelledby="headingPages"
      data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="">UP3</a>
        <a class="collapse-item" href="">ULP</a>
      </div>
    </div>
  </li>
  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">
  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>
</ul>