@extends('base')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-2">
		<h1 class="h3 mb-0 text-gray-800">Cari Titik Ukur</h1>
		<form class="d-none d-sm-inline-block form-inline ml-auto ml-md-3 my-2 my-md-2 mw-100" method="POST"
			action="javascript:search()" name="search" style="width: 50%;">
			{{ csrf_field() }}
			<div class="input-group">
				<input type="text" id="insearch" name="insearch" class="form-control"
					placeholder="Masukkan nama / kode titik ukur" aria-label="Search" aria-describedby="basic-addon2">
				<div class="input-group-append">
					<button class="btn btn-primary" type="submit" id="search">
						<i class="fas fa-search fa-sm"></i>
					</button>
				</div>
			</div>
		</form>
	</div>
	<div class="card" id="result" style="visibility:visible">
		<div class="card-header py-3" id="search-header">
			<h6 class="m-0 font-weight-bold text-primary">Hasil Aset : -</h6>
		</div>
		<div class="card-body">
			<div class="row" id="result">
				<div class="col col-lg-9 col-md-9 col-sm-9">
					<canvas id="myChart"></canvas>
				</div>
				<div class="col col-lg-3 col-md-3 col-sm-3">
					<div class="card shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary">Statistik Aset</h6>
						</div>
						<div class="card-body" style="padding: 0.75rem 0.75rem 0.2rem 1rem;">
							<div class="row">
								<div class="card border-left-info shadow h-90 py-0 mx-2 mb-2" style="width: 90%">
									<div class="card-body" style="padding: 0.75rem 0.75rem 0.75rem 1rem;">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="text-xs font-weight-bold text-info text-uppercase mb-0">
													kWh Produksi
												</div>
												<div class="text-xs font-weight-bold text-grey-700 text-uppercase mb-1">
													44.561 kWh</div>
												<div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="card border-left-warning shadow h-90 py-0 mx-2 mb-2" style="width: 90%">
									<div class="card-body" style="padding: 0.75rem 0.75rem 0.75rem 1rem;">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="text-xs font-weight-bold text-warning text-uppercase mb-0">
													Jumlah Sub-Aset
												</div>
												<div class="text-xs font-weight-bold text-grey-700 text-uppercase mb-1">
													30 bh</div>
												<div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="card border-left-danger shadow h-90 py-0 mx-2 mb-2" style="width: 90%">
									<div class="card-body" style="padding: 0.75rem 0.75rem 0.75rem 1rem;">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="text-xs font-weight-bold text-danger text-uppercase mb-0">
													Losses (kWh)
												</div>
												<div class="text-xs font-weight-bold text-grey-700 text-uppercase mb-1">
													561 kWh</div>
												<div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="d-flex justify-content-center mt-3" style="width: 100%">
					<div class="card p-2" style="width: 100%">
						<div class="table-responsive">
							<caption>
								<h1 class="h5 mb-2 text-gray-800 text-center">History Produksi</h3>
							</caption>
							<table class="table table-bordered" id="pembebanan_siang" name="pembebanan_siang"
								width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>No</th>
										<th>Tanggal</th>
										<th>Jam</th>
										<th>Stand kWh WBP</th>
										<th>Stand kWh LWBP</th>
										<th>Total kWh</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<td></td>
										<td><a></a></td>
										<td><a></a></td>
										<td><a></a></td>
										<td><a></a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</div>
	<script>
		$(function () {
	      const ctx = document.getElementById('myChart');
	      
	      new Chart(ctx, {
	          type: 'line',
	          data: {
	          labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun'],
	          datasets: [{
				label: 'Losses (%)',
				data: [0, 0, 0, 0, 0, 0],
				borderWidth: 1,
				borderColor: '#00628B',
				pointBackgroundColor: '#00628B',
	        }]
	      },
	      options: {
	        scales: {
	            y: {
	              beginAtZero: true
	            }
	          }
	        }
	      });
	    })
	</script>
</div>
@endsection