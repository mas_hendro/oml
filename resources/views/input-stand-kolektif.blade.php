@extends('base')
@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Input Stand - Kolektif</h1>
        <a href="" class="d-none d-sm-inline-block btn btn-sm btn-warning shadow-sm"><i
                class="fas fa-download fa-sm text-white"></i> Template File Upload</a>
    </div>
    <!-- Page Body -->
    <div class="row">
        <div class="card" style="width: 100%">
            <div class="card-body">
                <button class="btn btn-sm btn-primary btn-icon-split mb-4" data-toggle="modal"
                    data-target="#importDataGardu">
                    <span class="icon text-white">
                        <i class="fas fa-file"></i>
                    </span>
                    <span class="text"> Pilih File</span>
                </button>
                @if (empty($dataGardu))
                @else
                <a name="alert-success" class="btn btn-sm btn-success btn-icon-split mb-4"
                    href="javascript:saveGardu('{{ json_encode($dataGardu[0])}}');">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text text-white"> Simpan</span>
                </a>
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Kode Gardu</th>
                                <th>Nama Gardu</th>
                                <th>Aset Sebelum</th>
                                <th>Aset Sesudah</th>
                                <th>Koordinat X</th>
                                <th>Koordinat Y</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($dataGardu as $gardu)
                            @foreach ($gardu as $element)
                            <tr>
                                <td>{{ $element['ulp'] }}</td>
                                <td>{{ $element['nama_gardu'] }}</td>
                                <td>{{ $element['penyulang'] }}</td>
                                <td>{{ $element['tipe'] }}</td>
                                <td>{{ $element['merk'] }}</td>
                                <td>{{ $element['daya'] }}</td>
                            </tr>
                            @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@endsection