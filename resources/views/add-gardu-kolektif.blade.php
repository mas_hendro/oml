@extends('base')
@section('content')
<div class="container-fluid">
	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Aset Gardu - Kolektif</h1>
		<a href="{{ route('gardu.format-file') }}" class="d-none d-sm-inline-block btn btn-sm btn-warning shadow-sm"><i
				class="fas fa-download fa-sm text-white"></i> Template File Upload</a>
	</div>
	<!-- Page Body -->
	<div class="row">
		<div class="card" style="width: 100%">
			<div class="card-body">
				<button class="btn btn-sm btn-primary btn-icon-split mb-4" data-toggle="modal"
					data-target="#importDataGardu">
					<span class="icon text-white">
						<i class="fas fa-file"></i>
					</span>
					<span class="text"> Pilih File Gardu</span>
				</button>
				@if (empty($dataGardu))
				@else
				<a name="alert-success" class="btn btn-sm btn-success btn-icon-split mb-4" href="">
					<span class="icon text-white-50">
						<i class="fas fa-save"></i>
					</span>
					<span class="text text-white"> Simpan</span>
				</a>
				<div class="table-responsive">
					<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>Unit UL</th>
								<th>Kode Gardu</th>
								<th>Nama Gardu</th>
								<th>Daya</th>
								<th>FKM</th>
								<th>Lat</th>
								<th>Long</th>
								<th>Meter / No Meter</th>
								<th>Before</th>
								<th>After</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($dataGardu as $gardu)
							@foreach ($gardu as $element)
							<tr>
								<td>{{ $element['unitul'] }}</td>
								<td>{{ $element['kode_gardu']}}</td>
								<td>{{ $element['nama_gardu']}}</td>
								<td>{{ $element['daya']}}</td>
								<td>{{ $element['fkm']}}</td>
								<td>{{ $element['merk_meter_terpasang']}} / {{ $element['no_meter_terpasang']}}</td>
								<td>{{ $element['lat']}}</td>
								<td>{{ $element['long']}}</td>
								<td>{{ $element['aset_sebelum']}}</td>
								<td>{{ $element['aset_sesudah']}}</td>
							</tr>
							@endforeach
							@endforeach
						</tbody>
					</table>
				</div>
				@endif
			</div>
		</div>
	</div>
</div>
<!-- /.container-fluid -->
@endsection