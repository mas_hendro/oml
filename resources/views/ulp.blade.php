@extends('base')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">ULP {{$ulp->nama}}</h1>
  <p class="mb-4">Daftar gardu yang dimiliki oleh ULP {{$ulp->nama}}</p>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Daftar Gardu</h6>
    </div>
    <div class="card-body">
      @if($gardu->count() < 1) <div class="card-body">
        Data GARDU belum dimasukkan di ULP ini.
    </div>
    @else
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Penyulang</th>
            <th>Nama Gardu</th>
            <th>Tipe Gardu</th>
            <th>Daya Trafo</th>
            <th>Pengukuran Siang</th>
            <th>Pengukuran Malam</th>
            <th>Petugas Ukur</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($gardu as $key => $item):?>
          <tr>
            <td>{{++$key}}</td>
            <td>{{$item->penyulang}}</td>
            <td>{{$item->nama_gardu}}</td>
            <td>{{$item->tipe_gardu}}</td>
            <td>{{$item->trafo->daya}}</td>
            <td>
              <?php $pengukuran = App\Gardu::getLastPengukuran($item->id, 'siang'); ?>
              {{ $pengukuran == null ? 'Belum diukur':Carbon\Carbon::parse($pengukuran->tgl_pengukuran)->formatLocalized('%A, %d %b %Y %H:%M:%S') }}
            </td>
            <td>
              <?php $pengukuran = App\Gardu::getLastPengukuran($item->id, 'malam'); ?>
              {{ $pengukuran == null ? 'Belum diukur':Carbon\Carbon::parse($pengukuran->tgl_pengukuran)->formatLocalized('%A, %d %b %Y %H:%M:%S') }}
            </td>
            <td>{{ empty($user[--$key]->name) ? '':$user[$key]->name }}</td>
            <td>
              <a href="{{url('/gardu/'.$item->id.'')}}" class="btn btn-primary btn-circle btn-sm" title="detail">
                <i class="fas fa-eye"></i>
              </a>
              <a href="javascript:deleteGardu({{$item->id}})" class="btn btn-danger btn-circle btn-sm mt-1" title="hapus">
                <i class="fas fa-trash"></i>
              </a>
            </td>
          </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
    @endif
  </div>
</div>
</div>
<!-- /.container-fluid -->
@endsection