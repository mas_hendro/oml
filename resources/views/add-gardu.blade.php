@extends('base')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-4 text-gray-800">Aset Gardu</h1>

	{{-- Page Body --}}
	<div class="row">
		<div class="card" style="width: 100%">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">Tambah Aset Gardu</h6>
			</div>
			<div class="card-body container">
				<form name="add_gardu_form" id="add_gardu_form">
					<div class="row">
						{{ csrf_field() }}
						<div class="col">
							<div class="row mb-3">
								<input class="form-control" id="code" name="code" type="text"
									placeholder="kode gardu - daya (TBL032 - 150)">
							</div>
							<div class="row mb-3">
								<input class="form-control" id="name" name="name" type="text" placeholder="nama gardu">
							</div>
							<div class="row mb-3">
								<input class="form-control" id="lat" name="lat" type="text" placeholder="koordinat X">
							</div>
							<div class="row mb-3">
								<input class="form-control" id="asset_before" name="asset_before" type="text"
									placeholder="kode aset sebelum (TBL031)">
							</div>
						</div>
						<div class="col ml-3">
							<div class="row mb-3">
								<input class="form-control" id="merkmeter" name="merkmeter" type="text"
									placeholder="merk meter">
							</div>
							<div class="row mb-3">
								<input class="form-control" id="nometer" name="nometer" type="text"
									placeholder="no meter - FKM">
							</div>
							<div class="row mb-3">
								<input class="form-control" id="lng" name="lng" type="text" placeholder="koordinat Y">
							</div>
							<div class="row mb-3">
								<input class="form-control" id="asset_next" name="asset_next" type="text"
									placeholder="kode aset setelah (TBL033) - diisi jika ada">
							</div>
						</div>
					</div>
				</form>
				<button class="btn btn-sm btn-success shadow-sm" type="submit" form="add_gardu_form"
					onclick="javascript: add_gardu_form.action='{{ url('titikukur/add/gardu/satuan/true')}}'; add_gardu_form.method='post';">
					<span class="icon text-white-50">
						<i class="fas fa-save fa-sm text-white"></i>
					</span>
					<span class="text"> Simpan</span>
				</button>
				@if (Request::is('titikukur/add/gardu/satuan/true') == True)
				@if ($stat == True)
				<script type="text/javascript">
					Swal.fire({
						title: "Data Aset Gardu",
						text: "Data berhasil disimpan !",
						icon: "success",
					}).then((value) => {
						window.location.href = "/titikukur/add/gardu/satuan"
					});
				</script>
				@else
				<script type="text/javascript">
					Swal.fire({
						title: "Data Aset Gardu",
						text: "Data gagal disimpan !",
						icon: "error",
					}).then((value) => {
						window.location.href = "/titikukur/add/gardu/satuan"
					});
				</script>
				@endif
				@endif
			</div>
		</div>
	</div>
</div>
<!-- /.container-fluid -->

@endsection