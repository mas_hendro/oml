@extends('base')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-2">
        <h1 class="h3 mb-0 text-gray-800">Input Stand kWh - Satuan</h1>
    </div>
    <div class="card" id="result" style="visibility:visible">
        <div class="card-header py-2 px-4" id="search-header">
            <div class="row d-sm-flex align-items-center justify-content-between">
                <h6 class="m-0 ml-2 font-weight-bold text-primary">Titik Ukur : -</h6>
                <div class="d-sm-flex align-items-end" style="width: 50%">
                    <form class="d-none d-sm-inline-block form-inline" method="POST" action="javascript:search()"
                        name="search" style="width: 100%">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="text" id="stand-search" name="stand-search" class="form-control"
                                placeholder="Masukkan nama / kode titik ukur" aria-label="Search"
                                aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit" id="search">
                                    <i class="fas fa-search fa-sm"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form class="form-group">
                <div class="row">
                    <div class="col">
                        <div class="row mb-3">
                            <input class="form-control" id="id-titik-ukur" type="text"
                                placeholder="jenis - kode - nama titik ukur" readonly>
                        </div>
                        <div class="row mb-0">
                            <input class="form-control" id="stand-lalu" type="text" placeholder="stand lalu : "
                                readonly>
                        </div>
                    </div>
                    <div class="col ml-3">
                        <div class="row mb-3">
                            <input class="form-control" id="tanggal-ukur" type="text" placeholder="tanggal ukur"
                                readonly>
                        </div>
                        <div class="row mb-0">
                            <input class="form-control" id="stand-saat-ini" type="text"
                                placeholder="input stand saat ini">
                        </div>
                    </div>
                </div>
            </form>
            <div class="d-flex justify-content-end">
                <button class="btn btn-sm btn-success shadow-sm" type="submit" form="hidden_me" onclick="">
                    <span class="icon text-white-50">
                        <i class="fas fa-save fa-sm text-white"></i>
                    </span>
                    <span class="text"> Simpan</span>
                </button>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
</div>
@endsection