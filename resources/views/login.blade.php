<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AMG-UP3 Cikarang</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
</head>
<body>
    <!-- <link rel="stylesheet" type="text/css" href="{{'//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'}}" rel="stylesheet" id="bootstrap-css"> -->
    <link rel="stylesheet" type="text/css" href="{{url('/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/css/login.css')}}">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <div class="wrapper fadeInDown">
      <div id="formContent">
        <!-- Tabs Titles -->

        <!-- Icon -->
        <div class="fadeIn first">
            <h1 style="margin-top: 10px">AMG</h1>
        </div>

        <!-- Login Form -->
        <form action="{{url('loginPost')}}" method="post">
          {{csrf_field()}}
          <input type="text" id="username" class="fadeIn second" name="username" placeholder="username">
          <input type="password" id="password" class="fadeIn third" name="password" placeholder="password">
          <input type="submit" class="fadeIn fourth" value="Log In">
      </form>

      <div class="afooter">
        <h6>Aplikasi Manajemen Gardu</h6>
        <h6>Copyright @ 2019 PT PLN UP3 Cikarang</h6>
    </div>

</div>
</div>
</body>
</html>
