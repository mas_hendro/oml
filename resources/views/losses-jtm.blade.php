@extends('base')
@section('content')
<div class="container-fluid">
	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-2">
		<h1 class="h3 mb-0 text-gray-800">Analisa dan Evaluasi</h1>
	</div>
	<!-- Page Body -->
	<div class="row">
		<div class="card" style="width: 100%">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">Losses JTM s/d Gardu</h6>
			</div>
			<div class="card-body">
				<form>
					<div class="row">
						<div class="col">
							<select class="form-control" id="penyulang">
								<option selected disabled>Pilih Penyulang</option>
								<option value="1">Kota</option>
								<option value="0">Bupati</option>
							</select>
						</div>
						<div class="col">
							<button class="btn btn-sm btn-info shadow-sm" type="submit" onclick="">
								<span class="icon text-white-50">
									<i class="fas fa-eye fa-sm text-white"></i>
								</span>
								<span class="text"> Tampilkan</span>
							</button>
						</div>
					</div>
				</form>
				<div class="row">
					<div class="mt-3 col-xl-9 col-md-9 col-sm-9" id="map"></div>
					<div class="mt-3 col-xl-3 col-md-3 col-sm-3">
						<div class="card border-left-success shadow py-1">
							<div class="card-body">
								<div class="row no-gutters align-items-center">
									<div class="col mr-2">
										<div class="text-xs font-weight-bold text-success text-uppercase mb-1">kWh
											Produksi
										</div>
										<div class="h5 mb-0 font-weight-bold text-gray-800">6.514.221 kWh</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card border-left-warning shadow py-1 mt-1">
							<div class="card-body">
								<div class="row no-gutters align-items-center">
									<div class="col mr-2">
										<div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
											Total kWh Gardu</div>
										<div class="h5 mb-0 font-weight-bold text-gray-800">6.417.222 kWh</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card border-left-danger shadow py-1 mt-1">
							<div class="card-body">
								<div class="row no-gutters align-items-center">
									<div class="col mr-2">
										<div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
											Losses</div>
										<div class="h5 mb-0 font-weight-bold text-gray-800">98.114 kWh</div>
									</div>
								</div>
							</div>
						</div>
						<button class="btn btn-sm btn-warning shadow-sm mt-2" type="submit" form="hidden_me"
							onclick="javascript: hidden_me_siang.action=''; hidden_me_siang.method='post';">
							<span class="icon text-white-50">
								<i class="fas fa-redo fa-sm text-white"></i>
							</span>
							<span class="text"> Reset</span>
						</button>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="modal" id="modalListGardu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"></h5>
			</div>
			<div id="modal-javascript"></div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$("#map").css("height", "{{(400)}}px")
</script>
<!-- /.container-fluid -->
@endsection