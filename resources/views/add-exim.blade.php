@extends('base')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Aset Penyulang / Exim</h1>

    {{-- Page Body --}}
    <div class="row">
        <div class="card" style="width: 100%">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Tambah Aset Penyulang / kWh Exim</h6>
            </div>
            <div class="card-body container">
                <form id="add_exim_form" name="add_exim_form">
                    <div class="row">
                        {{ csrf_field() }}
                        <div class="col">
                            <div class="row mb-3">
                                <label for="name">Nama Penyulang / Exim :</label>
                                <input class="form-control" id="name" type="text" name="name"
                                    placeholder="Nama Penyulang / kWh Exim">
                            </div>
                            <div class="row mb-3">
                                <label>Jenis : </label>
                                <select class="form-control" id="type" name="type">
                                    <option value="1">Penyulang</option>
                                    <option value="0">kWh Exim</option>
                                </select>
                            </div>
                            <div class="row mb-3">
                                <label for="min_1">Aset Sebelum :</label>
                                <input class="form-control" id="min_1" type="text" name="min_1"
                                    placeholder="Kode aset sebelum aset ini">
                            </div>
                        </div>
                        <div class="col ml-3">
                            <div class="row mb-3">
                                <label for="x_cord">Koordinat X :</label>
                                <input class="form-control" id="x_cord" name="x_cord" type="text"
                                    placeholder="Koordinat X">
                            </div>
                            <div class="row mb-3">
                                <label for="y_cord">Koordinay Y : </label>
                                <input class="form-control" id="y_cord" name="y_cord" type="text"
                                    placeholder="Koordinat Y">
                            </div>
                            <div class="row mb-3">
                                <label for="plus_1">Aset Sesudah :</label>
                                <input class="form-control" id="plus_1" type="text" name="plus_1"
                                    placeholder="Kode aset sesudah aset ini">
                            </div>
                        </div>
                    </div>
                </form>
                <button class="btn btn-sm btn-success shadow-sm" type="submit" form="add_exim_form"
                    onclick="javascript: add_exim_form.action='{{ url('titikukur/add/exim/save/true')}}'; add_exim_form.method='post';">
                    <span class="icon text-white-50">
                        <i class="fas fa-save fa-sm text-white"></i>
                    </span>
                    <span class="text"> Simpan</span>
                </button>
                @if (Request::is('titikukur/add/exim/save/true') == True)
                @if ($stat == True)
                <script type="text/javascript">
                    Swal.fire({
                        title: "Data Aset Exim",
                        text: "Data berhasil disimpan !",
                        icon: "success",
                    }).then((value) => {
                        window.location.href = "/titikukur/add/exim"
                    });
                </script>
                @else
                <script type="text/javascript">
                    Swal.fire({
                        title: "Data Aset Exim",
                        text: "Data gagal disimpan !",
                        icon: "error",
                    }).then((value) => {
                        window.location.href = "/titikukur/add/exim"
                    });
                </script>
                @endif
                @endif
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

@endsection