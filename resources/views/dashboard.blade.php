@extends('base')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Kondisi Losses Sistem</h1>
  </div>
  {{-- {{ dd($total) }} --}}
  <!-- Status Losses UP3 -->
  <div class="col-auto">
    <div class="d-sm-flex align-items-center justify-content-between mb-2">
      <h3 class="h5 mb-0 text-gray-800">Losses Terbesar</h3>
    </div>
    <div class="row">
      <!-- Penyulang -->
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Penyulang</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">Togoli</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Losses Penyulang -->
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Losses Penyulang</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">4.471 kWh</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Gardu -->
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Nama Gardu</div>
                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">TBL031A</div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Losses Gardu -->
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Losses Gardu</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">954 kWh</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <!-- Losses per ULP -->
  <div class="col-auto">
    @for ($i = 0; $i < 1; $i++) <div class="col-auto">
      <div class="d-sm-flex align-items-center justify-content-between mb-2">
        <h1 class="h5 mb-0 text-gray-800">ULP Tobelo</h1>
      </div>
      <div class="row">
        <!-- Status Gardu -->
        <div class="col-xl-7 col-lg-7 col-md-5 col-sm-5">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">History Losses Per Sistem</h6>
            </div>
            <div class="card-body">
              <div>
                <canvas id="myChart"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Status Losses Terbesar pada Sistem ULP</h6>
            </div>
            <div class="card-body" style="padding: 0.75rem 0.75rem 0.2rem 1rem;">
              <div class="row">
                <div class="col-xl-6 col-md-6 mb-2">
                  <div class="card border-left-info shadow h-90 py-0">
                    <div class="card-body" style="padding: 0.75rem 0.75rem 0.75rem 1rem;">
                      <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                          <div class="text-xs font-weight-bold text-info text-uppercase mb-0">Losses Penyulang</div>
                          <div class="text-xs font-weight-bold text-grey-700 text-uppercase mb-1">44.561 kWh</div>
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-6 col-md-6 mb-2">
                  <div class="card border-left-warning shadow h-90 py-0">
                    <div class="card-body" style="padding: 0.75rem 0.75rem 0.75rem 1rem;">
                      <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                          <div class="text-xs font-weight-bold text-warning text-uppercase mb-0">Losses Penyulang</div>
                          <div class="text-xs font-weight-bold text-grey-700 text-uppercase mb-1">44.561 kWh</div>
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-xl-6 col-md-6 mb-2">
                  <div class="card border-left-info shadow h-90 py-0">
                    <div class="card-body" style="padding: 0.75rem 0.75rem 0.75rem 1rem;">
                      <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                          <div class="text-xs font-weight-bold text-info text-uppercase mb-0">Losses Gardu
                          </div>
                          <div class="text-xs font-weight-bold text-grey-700 text-uppercase mb-1">TBL031A</div>
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-6 col-md-6 mb-2">
                  <div class="card border-left-warning shadow h-90 py-0">
                    <div class="card-body" style="padding: 0.75rem 0.75rem 0.75rem 1rem;">
                      <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                          <div class="text-xs font-weight-bold text-warning text-uppercase mb-0">Losses Gardu</div>
                          <div class="text-xs font-weight-bold text-grey-700 text-uppercase mb-1">6.541 kWh
                          </div>
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
  @endfor
  <!-- /.container-fluid -->
  <script>
    $(function () {
      const ctx = document.getElementById('myChart');
      
      new Chart(ctx, {
          type: 'line',
          data: {
          labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
          datasets: [{
          label: '# of Votes',
          data: [12, 19, 3, 5, 2, 3],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
            y: {
              beginAtZero: true
            }
          }
        }
      });
    })
  </script>
</div>
@endsection