<a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{url('/')}}">
  <div class="sidebar-brand-icon">
    <i class="fas fa-cube"></i>
  </div>
  <div class="sidebar-brand-text mx-3">OML</div>
</a>